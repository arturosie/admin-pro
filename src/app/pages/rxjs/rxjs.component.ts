import { Component, OnDestroy } from '@angular/core';
import { Observable, interval, Subscription } from 'rxjs';
import { retry, take, map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: [
  ]
})
export class RxjsComponent implements OnDestroy {

  public intervalSubs: Subscription;

  constructor() {

    // this.retornaObservable().pipe(
    //   retry(1)
    // ).subscribe(
    //   valor => console.log('Subs: ', valor),
    //   error => console.error('Error: ', error),
    //   () => console.info('Completado')
    // );

    this.intervalSubs = this.retornaIntervalo()
                          .subscribe( console.log )

  }

  ngOnDestroy(): void {
    this.intervalSubs.unsubscribe();
  }

  retornaIntervalo(): Observable<number> {

    return interval(500)
            .pipe(
              //take(10),
              map( valor => valor + 1 ),
              filter( valor => ( valor % 2 == 0)? true: false ),
            );

  }

  retornaObservable(): Observable<number> {
    let i = 0;

    return new Observable<number>( observe => {

      const intervalo = setInterval( () => {

        i++;
        observe.next(i);

        if ( i == 4) {
          clearInterval( intervalo );
          observe.complete();
        }

        if( i == 2 ) {
          observe.error('i llego al valor de 2');
        }

      }, 1000 )

    });

  }

}
