import { Component, OnInit, OnDestroy } from '@angular/core';
import Swal from 'sweetalert2';

import { UsuarioService } from '../../../services/usuario.service';
import { ModalImagenService } from '../../../services/modal-imagen.service';
import { BusquedasService } from '../../../services/busquedas.service';

import { Usuario } from 'src/app/models/usuario.model';
import { delay } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styles: [
  ]
})
export class UsuariosComponent implements OnInit, OnDestroy {

  public totalUsuarios: number = 0;
  public usuarios: Usuario[] = [];
  public usuariosTmp: Usuario[] = [];
  public pagina: number = 0;
  public cargando: boolean = true;
  public imgSubs: Subscription;

  constructor(private usuarioService: UsuarioService,
              private busquedasService: BusquedasService,
              private modalImagenService: ModalImagenService) { }

  ngOnInit(): void {

    this.cargarUsuarios();

    this.imgSubs = this.modalImagenService.nuevaImagen
          .pipe(
            delay(500)
          )
          .subscribe( img => this.cargarUsuarios() );

  }

  ngOnDestroy(): void {
    this.imgSubs.unsubscribe();
  }

  cargarUsuarios() {
    this.cargando = true;

    this.usuarioService.cargarUsuarios( this.pagina )
        .subscribe( ({ total, usuarios }) => {
          this.totalUsuarios = total;
          this.usuarios = usuarios;
          this.usuariosTmp = usuarios;
          this.cargando = false;
        })
  }

  cambiarPagina( valor: number ) {
    this.pagina += valor;

    if (this.pagina < 0) {
      this.pagina = 0;
    } else if ( this.pagina > this.totalUsuarios ) {
      this.pagina -= valor;
    }

    this.cargarUsuarios();
  }

  buscar( termino: string ) {

    if ( termino.length === 0) {
      return this.usuarios = this.usuariosTmp;
    }

    this.busquedasService.buscar( 'usuarios', termino )
          .subscribe( (resultados: Usuario[]) => {
            this.usuarios = resultados;
          })
  }

  eliminarUsuario( usuario: Usuario) {

    if( usuario.uid === this.usuarioService.getUid ) {
      return Swal.fire('Error', 'No puede borrarse a si mismo', 'error');
    }

    Swal.fire({
      title: '¿Estas seguro?',
      text: `Esta apunto de borrar a ${ usuario.nombre }`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: '¡Si, hazlo!'
    }).then((result) => {
      if (result.value) {

        this.usuarioService.elimiarUsuario( usuario )
            .subscribe( resp => {
              this.cargarUsuarios();
              Swal.fire(
              '¡Borrado!',
              `El usuario ${ usuario.nombre } a sido borrado`,
              'success'
              );
            }
            )        
      }
    })
  }

  cambiarRole( usuario: Usuario ) {
    this.usuarioService.guardarUsuario( usuario )
          .subscribe( resp => {
            console.log(resp);
          })
  }

  abrirModal(usuario: Usuario) {
    this.modalImagenService.abrirModal('usuarios', usuario.uid, usuario.img);
  }

}
