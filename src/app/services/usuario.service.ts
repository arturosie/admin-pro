import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';

import { environment } from 'src/environments/environment';

import { LoginForm } from '../interfaces/login-form.interface';
import { RegisterForm } from '../interfaces/register-form.interface';
import { CargarUsuario } from '../interfaces/cargar-usuarios.interfaces';
import { Usuario } from '../models/usuario.model';

declare const gapi: any;

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  public auth2: any;
  public usuario: Usuario;

  constructor(private http: HttpClient,
              private router: Router,
              private ngZone: NgZone) {
                this.googleInit();
  }

  get getToken():string {
    return localStorage.getItem('token') || '';
  }

  get getUid():string {
    return this.usuario.uid || '';
  }

  get getRole(): 'ADMIN_ROLE' | 'USER_ROLE' {
    return this.usuario.role;
  }

  get getHeaders() {
    return {
      headers: {
        'x-token': this.getToken
      }
    }
  }

  guardarLocalStorage( token: string, menu: any ) {
    localStorage.setItem('token', token);
    localStorage.setItem('menu', JSON.stringify(menu) );
  }

  googleInit() {

    return new Promise (resolve => {
      
      gapi.load('auth2', () => {
        this.auth2 = gapi.auth2.init({
          client_id: '895436383115-nibigctlm3ralspnkrplhjrhn3fbjqgk.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin',
        });

        resolve();
      });

    })
  }
  
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('menu');
    this.auth2.signOut().then( () => {
      this.ngZone.run(() => {
        this.router.navigateByUrl('/login');
      })
    });

  }

  validarToken(): Observable<boolean> {

    return this.http.get(`${ base_url }/login/renew`, {
      headers: {
        'x-token': this.getToken
      }
    }).pipe(
        map( (resp: any) => {
          const { email, google, nombre, role, img = '', uid } = resp.usuario;
          this.usuario = new Usuario( nombre, email, '', img, google, role, uid );
          
          this.guardarLocalStorage( resp.token, resp.menu );
          return true;
        }),
        catchError( error => of(false) )
    );
  }

  crearUsuario( formData: RegisterForm ) {

    return this.http.post(`${ base_url }/usuarios`, formData )
              .pipe(
                tap( (resp: any) => this.guardarLocalStorage( resp.token, resp.menu ) )
              );

  }

  actualizarPerfil( data: {email: string, nombre: string, role: string} ) {

    data = {
      ...data,
      role: this.usuario.role
    }

    return this.http.put(`${ base_url }/usuarios/${ this.getUid }`, data, this.getHeaders );

  }

  login( formData: LoginForm ) {

    return this.http.post(`${ base_url }/login`, formData )
              .pipe(
                tap( (resp: any) => this.guardarLocalStorage( resp.token, resp.menu ) )
              );

  }

  loginGoogle( token ) {

    return this.http.post(`${ base_url }/login/google`, { token } )
              .pipe(
                tap( (resp: any) => this.guardarLocalStorage( resp.token, resp.menu ) )
              );

  }

  cargarUsuarios(desde: number = 0) {

    const url = `${ base_url}/usuarios?desde=${ desde }`;

    return this.http.get<CargarUsuario>(url, this.getHeaders)
                  .pipe(
                    map( resp => {
                      const usuarios = resp.usuarios.map( 
                        user => new Usuario(user.nombre, user.email, '', user.img, user.google, user.role, user.uid)
                      );


                      return {
                        total: resp.total,
                        usuarios
                      };
                    })
                  )

  }

  elimiarUsuario( usuario: Usuario ) {
    const url = `${ base_url}/usuarios/${ usuario.uid }`;

    return this.http.delete(url, this.getHeaders);
  }

  guardarUsuario( usuario: Usuario ) {

    return this.http.put(`${ base_url }/usuarios/${ usuario.uid }`, usuario, this.getHeaders );

  }

}
